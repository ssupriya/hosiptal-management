import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the FilterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()

export class FilterProvider {
  states:any[];

  constructor() {
    console.log('Hello FilterProvider Provider');
  }
  initializeState() {
    return new Promise(resolve => {
      this.states = [
        { id: 1, name: 'Ananthapur' },
        { id: 2, name: 'Chittoor' },
        { id: 3, name: 'East Godavari' },
        { id: 4, name: 'Guntur' },
        { id: 5, name: 'Kadapa' },
        { id: 6, name: 'Krishna' },
        { id: 7, name: 'Kurnool' },
        { id: 8, name: 'Nellore' },
        { id: 9, name: 'Prakasam' },
        { id: 10, name: 'Srikakulam' },
        { id: 11, name: 'Visakhapatnam' },
        { id: 12, name: 'Vizianagaram' },
        { id: 13, name: 'West Godavari' },
      ];
      resolve(this.states);
    });
  }
 
}
