import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ActionSheetController } from 'ionic-angular';

@Injectable()
export class CameraProvider {

 constructor(private camera: Camera, public actionSheetCtrl: ActionSheetController) {
   console.log('Hello CameraProvider Provider');
 }

 // Selecting Image from Gallery or Take a New Photo
 selectImage() {
   return new Promise((resolve) => {
     let actionSheet = this.actionSheetCtrl.create({
       title: 'Select Image Source',
       buttons: [
         {
           text: 'Load from Library',
           handler: () => {
             this.openGallery().then((res) => {
               resolve(res);
             });
           }
         },
         {
           text: 'Use Camera',
           handler: () => {
             this.takePicture().then((res) => {
               resolve(res);
             });
           }
         },
         {
           text: 'Cancel',
           role: 'cancel'
         }
       ]
     });
     actionSheet.present();

   });

 }

 // Choosing an Image from Gallery
 openGallery() {
   // Defining camera options
   const options: CameraOptions = {
     quality: 50,
     sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
     destinationType: this.camera.DestinationType.DATA_URL,
     encodingType: this.camera.EncodingType.JPEG,
     mediaType: this.camera.MediaType.PICTURE,
     targetWidth: 800,
     targetHeight: 480,
     allowEdit: true,
     correctOrientation: true
   }

   var base64ImageURL;

   return new Promise(resolve => {
     this.camera.getPicture(options).then((file_uri) => {
       // imageData is either a base64 encoded string or a file URI
       // If it's file URI:
       base64ImageURL = 'data:image/jpeg;base64,' + file_uri;
       resolve(base64ImageURL);
     },
       (err) => {
         console.log(err);
       });
   });

 }

 // Choosing an Image using Camera
 takePicture() {
   // Defining camera options
   const options: CameraOptions = {
     quality: 50,
     destinationType: this.camera.DestinationType.DATA_URL,
     encodingType: this.camera.EncodingType.JPEG,
     mediaType: this.camera.MediaType.PICTURE,
     targetWidth: 800,
     targetHeight: 480,
     allowEdit: true,
     correctOrientation: true
   }

   var base64ImageURL;

   return new Promise(resolve => {
     this.camera.getPicture(options).then((imageData) => {
       // imageData is either a base64 encoded string or a file URI
       // If it's base64:
       base64ImageURL = 'data:image/jpeg;base64,' + imageData;
       resolve(base64ImageURL);
     }, (err) => {
       // Handle error
       console.log(err);
     });
   });
 }


}


