import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddoctorPage } from './addoctor';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AddoctorPage,
  ],
  imports: [ReactiveFormsModule,FormsModule,
    IonicPageModule.forChild(AddoctorPage),
  ],
})
export class AddoctorPageModule {}
