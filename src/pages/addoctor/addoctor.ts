import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import 'firebase/storage';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
/**
 * Generated class for the AddoctorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addoctor',
  templateUrl: 'addoctor.html',
})
export class AddoctorPage {
  private db = firebase.firestore();
  private data: FormGroup;

  formgroup: FormGroup;
  email: AbstractControl;
  password: AbstractControl;
  users: any;    

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public formbuilder: FormBuilder,
    public menuCtrl: MenuController,

  ) {
    this.formgroup = formbuilder.group({
      firstname: ['', [Validators.required, Validators.minLength(3)]],
      lastname: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,3}")]],
      specialist: ['', [Validators.required, Validators.minLength(3)]],
      timings: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
    this.menuCtrl.enable(false, 'default');
    this.menuCtrl.enable(false, 'patient');
    this.menuCtrl.enable(false, 'doctor');
    this.menuCtrl.enable(true, 'admin');

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad AddoctorPage');
  }
  add() {
    console.log('data', this.data);
    this.db.collection("doctordata").doc().set({
      firstname: this.formgroup.value.firstname,
      lastname: this.formgroup.value.lastname,
      email: this.formgroup.value.email,
      specialist: this.formgroup.value.specialist,
      timings: this.formgroup.value.timings,
      password: this.formgroup.value.password
    })
    .then(function (){
      console.log("doctor data succesfully added")
    })
    .catch(function(error){
       console.log("error in writing data",error);
    });
    console.log(this.formgroup.value.email, this.formgroup.value.password, ' Fields');
    const confirm = this.alertCtrl.create({
      title: 'Confirmation',
      message: 'Do you want to add one more Doctor',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('No clicked');
            this.no();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Add clicked');
            this.yes();
          }
        }
      ]
    });
    confirm.present();
  }
  yes() {
    this.navCtrl.push('AddoctorPage');
  }
  no() {
    this.navCtrl.setRoot('ViewmyhospitalPage');
  }
}

