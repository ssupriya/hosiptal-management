import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';


/**
 * Generated class for the PatiententryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()      
@Component({
  selector: 'page-patiententry',
  templateUrl: 'patiententry.html',
})
export class PatiententryPage {

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
      public menuCtrl: MenuController) {
    this.menuCtrl.enable(false, 'default');
      this.menuCtrl.enable(true, 'patient');
      this.menuCtrl.enable(false, 'doctor');
      this.menuCtrl.enable(false, 'admin');
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad PatiententryPage');
  }

  search(){
    this.navCtrl.push('HossearchPage');
  }
  remedy(){
    this.navCtrl.setRoot('HomeremedyPage');
  }
}
