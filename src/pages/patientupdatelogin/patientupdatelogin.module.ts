import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientupdateloginPage } from './patientupdatelogin';

@NgModule({
  declarations: [
    PatientupdateloginPage,
  ],
  imports: [
    IonicPageModule.forChild(PatientupdateloginPage),
  ],
})
export class PatientupdateloginPageModule {}
