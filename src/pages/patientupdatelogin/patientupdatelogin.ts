import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import * as firebase from 'firebase/app';


/**
 * Generated class for the PatientupdateloginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var isLogin = false;

@IonicPage()
@Component({
  selector: 'page-patientupdatelogin',
  templateUrl: 'patientupdatelogin.html',
})
export class PatientupdateloginPage {
  formgroup:FormGroup;
  phonenumber: AbstractControl;
  private db = firebase.firestore();

  users: any;

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public menuCtrl: MenuController,
     public formbuilder :FormBuilder
     ) {
      this.formgroup = formbuilder.group({
        phonenumber:['',[Validators.required,Validators.minLength(10),Validators.maxLength(10),Validators.pattern("^[0-9_-]{10,10}$")]],
     });
    this.menuCtrl.enable(false, 'default');
    this.menuCtrl.enable(true, 'patient');
    this.menuCtrl.enable(false, 'doctor');
    this.menuCtrl.enable(false, 'admin');
    this.menuCtrl.enable(false, 'common');
    }
    ionViewDidLoad() {
    console.log('ionViewDidLoad PatientupdateloginPage');
  }

  async login() {

    console.log('form data', this.formgroup.value.email)
    // var em =  this.formgroup.value.email;
    // var ps =  this.formgroup.value.password;
    await this.db.collection("appointments").where("phonenumber", "==", this.formgroup.value.phonenumber)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          // doc.data() is never undefined for query doc snapshots
          console.log(doc.id, " => ", doc.data());
          isLogin = true;
        });
      })
      .catch(function (error) {
        console.log("Error getting documents: ", error);
      });

    if (isLogin) {
      this.navCtrl.setRoot('AdminentryPage');
    } else {
      console.log('No details');
      alert('no user')
    }
  }

  update(){
    this.navCtrl.setRoot('ViewmyhospitalPage');
  }


updateappointment(){
    this.navCtrl.push('UpdateappointmentPage');
  }


}