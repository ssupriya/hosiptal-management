import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewmyhospitalPage } from './viewmyhospital';

@NgModule({
  declarations: [
    ViewmyhospitalPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewmyhospitalPage),
  ],
})
export class ViewmyhospitalPageModule {}
