import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ViewmyhospitalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewmyhospital',
  templateUrl: 'viewmyhospital.html',
})
export class ViewmyhospitalPage {

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
  public menuCtrl : MenuController,
  ) {
    this.menuCtrl.enable(false, 'default');
    this.menuCtrl.enable(false, 'patient');
    this.menuCtrl.enable(false, 'doctor');
    this.menuCtrl.enable(true, 'admin');
  
  }
    

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewmyhospitalPage');
  }

  upadateHospital(){
    this.navCtrl.push('UpdatehospitalPage');
  }
     
  add(){
    this.navCtrl.push('AddoctorPage');
  }

 
}
