import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FirsthosPage } from './firsthos';

@NgModule({
  declarations: [
    FirsthosPage,
  ],
  imports: [
    IonicPageModule.forChild(FirsthosPage),
  ],
})
export class FirsthosPageModule {}
