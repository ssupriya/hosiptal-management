import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';

/**
 * Generated class for the ConfirmpwdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirmpwd',
  templateUrl: 'confirmpwd.html',
})
export class ConfirmpwdPage {

  constructor(public navCtrl: NavController,
     public navParams: NavParams, 
     public alertCtrl: AlertController,
     public menuCtrl: MenuController,
     ) {
      this.menuCtrl.enable(false, 'default');
      this.menuCtrl.enable(false, 'patient');
      this.menuCtrl.enable(true, 'doctor');
      this.menuCtrl.enable(false, 'admin');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmpwdPage');
  }
  showConfirm() {
    const confirm = this.alertCtrl.create({
      title: 'Confirmation',
      message: 'You have reset your Password Successfully',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Ok clicked');
            this.ok();
          }
        },
        
      ]
    });
    confirm.present();
  }
  ok(){
    this.navCtrl.setRoot('AppointmentlistPage');
  }
}
