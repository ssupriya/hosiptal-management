import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmpwdPage } from './confirmpwd';

@NgModule({
  declarations: [
    ConfirmpwdPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmpwdPage),
  ],
})
export class ConfirmpwdPageModule {}
