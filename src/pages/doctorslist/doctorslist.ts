import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { FirebaseListObservable, AngularFireDatabase } from 'angularfire2/database';
import firebase from 'firebase';

/**
 * Generated class for the DoctorslistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-doctorslist',
  templateUrl: 'doctorslist.html',
})
export class DoctorslistPage {
  // doctorListRef$ : FirebaseListObservable<doctorProfile[]>
  private db = firebase.firestore();

  doctorList:any =[];

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public menuCtrl : MenuController,
    private database: AngularFireDatabase
    ) {
      // this.doctorListRef$ = this.database.list('doctor-profile');
      this.menuCtrl.enable(false, 'default');
      this.menuCtrl.enable(false, 'patient');
      this.menuCtrl.enable(false, 'doctor');
      this.menuCtrl.enable(true, 'admin');
  
  }    

  ionViewDidLoad() {
    console.log('ionViewDidLoad DoctorslistPage');
    this.getDoctorList();
  }
  

 async getDoctorList(){
    var doctorlist =[]
  await  this.db.collection("doctordata").get().then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
          console.log(`${doc.id} => ${doc.data()}`);
          doctorlist.push(doc.data());
      });
  });
 this.doctorList = doctorlist;
  console.log(JSON.stringify(this.doctorList),' DoctorList');
  }

}
