import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase/app';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

/**
 * Generated class for the UpdateappointmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var isLogin = false;
@IonicPage()
@Component({
  selector: 'page-updateappointment',
  templateUrl: 'updateappointment.html',
})
export class UpdateappointmentPage {

  private db = firebase.firestore();
  formgroup: FormGroup;
  phonenumber : any;
  users: any;
  //loginT: any = '0';

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public formbuilder: FormBuilder,
     ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpadateappointmentPage');
  }
  async login() {

    console.log('form data', this.formgroup.value.email)
    // var em =  this.formgroup.value.email;
    // var ps =  this.formgroup.value.password;
    await this.db.collection("appointments").where("phonenumber", "==", this.formgroup.value.phonenumber)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          // doc.data() is never undefined for query doc snapshots
          console.log(doc.id, " => ", doc.data());
          isLogin = true;
        });
      })
      .catch(function (error) {
        console.log("Error getting documents: ", error);
      });

    if (isLogin) {
      this.navCtrl.setRoot('AdminentryPage');
    } else {
      console.log('No details');
      alert('no user')
    }
  }

  update(){
    this.navCtrl.setRoot('ViewmyhospitalPage');
  }
}

