import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateappointmentPage } from './updateappointment';

@NgModule({
  declarations: [
    UpdateappointmentPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateappointmentPage),
  ],
})
export class UpdateappointmentPageModule {}
