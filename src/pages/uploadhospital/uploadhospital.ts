import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { CameraProvider } from '../../providers/camera/camera';
/**
 * Generated class for the UploadhospitalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-uploadhospital',
  templateUrl: 'uploadhospital.html',
})
export class UploadhospitalPage {
  private currentNumber = 0;
  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public Camera:CameraProvider,
     public menuCtrl : MenuController,
     ) {
      
        this.menuCtrl.enable(false, 'default');
        this.menuCtrl.enable(false, 'patient');
        this.menuCtrl.enable(false, 'doctor');
        this.menuCtrl.enable(true, 'admin');
      
      
  }
  private increment () {
    this.currentNumber++;
  }
  private decrement () {
    this.currentNumber--;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadhospitalPage');
  }
opencam(){
this.Camera.selectImage().then(res =>{
console.log(res);
})

}

doc(){
  this.navCtrl.push('AddoctorPage');
}
upload(){
  this.navCtrl.push('ViewmyhospitalPage');
}
}
