import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UploadhospitalPage } from './uploadhospital';

@NgModule({
  declarations: [
    UploadhospitalPage,
  ],
  imports: [
    IonicPageModule.forChild(UploadhospitalPage),
  ],
})
export class UploadhospitalPageModule {}
