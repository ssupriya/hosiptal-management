import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewpatientPage } from './viewpatient';

@NgModule({
  declarations: [
    ViewpatientPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewpatientPage),
  ],
})
export class ViewpatientPageModule {}
