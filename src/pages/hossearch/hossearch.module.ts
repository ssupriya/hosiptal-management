import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HossearchPage } from './hossearch';

@NgModule({
  declarations: [
    HossearchPage,
  ],
  imports: [
    IonicPageModule.forChild(HossearchPage),
  ],
})
export class HossearchPageModule {}
