import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, MenuController } from 'ionic-angular';

/**
 * Generated class for the HossearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hossearch',
  templateUrl: 'hossearch.html',
})
export class HossearchPage {
  filterVal: any;
  data: any;
  search_key="";
  items: any[];

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public modalCtrl:ModalController,
     public menuCtrl: MenuController) {
    this.menuCtrl.enable(false, 'default');
    this.menuCtrl.enable(true, 'patient');
    this.menuCtrl.enable(false, 'doctor');
    this.menuCtrl.enable(false, 'admin');
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad HossearchPage');
  }
hos1(){
  this.navCtrl.push('FirsthosPage');
}

hos2(){
  this.navCtrl.push('SecondhosPage');
}

filterModal() {
  const modal = this.modalCtrl.create("FilterPage");
  modal.onDidDismiss(data => {
console.log(data);
this.filterVal=data.name;
console.log(this.filterVal)
if(data.name==undefined){
 this.filterVal=""
 console.log(this.filterVal)
}


this.data=this.filterVal
    console.log("Filter Value: " + this.filterVal);
this.search_key="";      // this.constituency = data.Constituency;
    // this.mandal = data.Mandal;
    // this.searchKey1 = '';
    this.items=[];

    // this.getData1();
  });
  modal.present();

}


}       
