import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeremedyPage } from './homeremedy';

@NgModule({
  declarations: [
    HomeremedyPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeremedyPage),
  ],
})
export class HomeremedyPageModule {}
