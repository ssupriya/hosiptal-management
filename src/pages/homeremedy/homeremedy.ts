import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';

/**
 * Generated class for the HomeremedyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-homeremedy',
  templateUrl: 'homeremedy.html',
})
export class HomeremedyPage {

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public menuCtrl: MenuController
      ) {
    this.menuCtrl.enable(false, 'default');
    this.menuCtrl.enable(true, 'patient');
    this.menuCtrl.enable(false, 'doctor');
    this.menuCtrl.enable(false, 'admin');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeremedyPage');
  }
  
}
