import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AboutimagePage } from './aboutimage';

@NgModule({
  declarations: [
    AboutimagePage,
  ],
  imports: [
    IonicPageModule.forChild(AboutimagePage),
  ],
})
export class AboutimagePageModule {}
