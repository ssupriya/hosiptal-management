import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl} from '@angular/forms';
/**
 * Generated class for the AppointmentlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-appointmentlist',
  templateUrl: 'appointmentlist.html',
})
export class AppointmentlistPage {
  formgroup: FormGroup;
  date:AbstractControl;
  
  constructor(public navCtrl: NavController,
     public navParams: NavParams,
    public menuCtrl: MenuController,
     public formbuilder:FormBuilder) {
      this.formgroup = formbuilder.group({
      date:['',[Validators.required,Validators.minLength(10)]]
      });
    this.menuCtrl.enable(false, 'default');
    this.menuCtrl.enable(false, 'patient');
    this.menuCtrl.enable(true, 'doctor');
    this.menuCtrl.enable(false, 'admin');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppointmentlistPage');
  }

}
