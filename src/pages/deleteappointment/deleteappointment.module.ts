import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeleteappointmentPage } from './deleteappointment';

@NgModule({
  declarations: [
    DeleteappointmentPage,
  ],
  imports: [
    IonicPageModule.forChild(DeleteappointmentPage),
  ],
})
export class DeleteappointmentPageModule {}
