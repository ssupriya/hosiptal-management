import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import * as firebase from 'firebase/app';


/**
 * Generated class for the DeleteappointmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-deleteappointment',
  templateUrl: 'deleteappointment.html',
})
export class DeleteappointmentPage {
  formgroup:FormGroup;
  phonenumber: AbstractControl;
  private db = firebase.firestore();
  email: any;
  users: any;

  constructor(public navCtrl: NavController,
     public navParams: NavParams,  
     public alertCtrl: AlertController,
     public menuCtrl: MenuController,
     public formbuilder :FormBuilder
     ) {
      this.formgroup = formbuilder.group({
        phonenumber:['',[Validators.required,Validators.minLength(10),Validators.maxLength(10),Validators.pattern("^[0-9_-]{10,10}$")]],
     });
      this.menuCtrl.enable(false, 'default');
      this.menuCtrl.enable(true, 'patient');
      this.menuCtrl.enable(false, 'doctor');
      this.menuCtrl.enable(false, 'admin');
  }
  ionViewDidLoad() {
    }

  deleteappointment() {
    console.log('ionViewDidLoad DeleteappointmentPage');
    var func=this.db.collection("appointments").where("phonenumber","==",this.formgroup.value.phonenumber);
    func.get().then(function(querySnapshot) {
      querySnapshot.forEach(function(doc) {
        doc.ref.delete();
      });
    });
    const confirm = this.alertCtrl.create({
      title: 'Confirmation',
      message: 'Appointment deleted successfully',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok clicked');
            this.ok();
          }
        },
      ]
    });
    confirm.present();
  }
  ok(){
    this.navCtrl.setRoot('PatiententryPage');
  }
}
