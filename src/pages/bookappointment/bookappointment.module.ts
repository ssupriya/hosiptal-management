import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookappointmentPage } from './bookappointment';

@NgModule({
  declarations: [
    BookappointmentPage,
  ],
  imports: [
    IonicPageModule.forChild(BookappointmentPage),
  ],
})
export class BookappointmentPageModule {}
