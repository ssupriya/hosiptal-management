import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import 'firebase/storage';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
/**
 * Generated class for the BookappointmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bookappointment',
  templateUrl: 'bookappointment.html',
})
export class BookappointmentPage {
  private db = firebase.firestore();
  private data: FormGroup;
  formgroup: FormGroup;

  constructor(public navCtrl: NavController,
     public navParams: NavParams, 
     public alertCtrl: AlertController,
     public formbuilder: FormBuilder,
     ) {
      this.formgroup = formbuilder.group({
        firstname: ['', [Validators.required, Validators.minLength(3)]],
        lastname: ['', [Validators.required, Validators.minLength(3)]],
       // email: ['', [Validators.required, Validators.pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,3}")]],
        disease: ['', [Validators.required, Validators.minLength(3)]],
        timings: ['', [Validators.required]],
        days: ['', [Validators.required]],
       phonenumber: ['', [Validators.required]],
      });
  }
     
  ionViewDidLoad() {
    console.log('ionViewDidLoad BookappointmentPage');
  }
  showConfirm() {
      console.log('data', this.data);
      this.db.collection("appointments").doc().set({
        firstname: this.formgroup.value.firstname,
        lastname: this.formgroup.value.lastname,
        phonenumber: this.formgroup.value.phonenumber,
        //email: this.formgroup.value.email,
        disease: this.formgroup.value.disease,
        days: this.formgroup.value.days,
        timings: this.formgroup.value.timings
      })
      .then(function (){
        console.log("doctor data succesfully added")
      })
      .catch(function(error){
         console.log("error in writing data",error);
      });
      console.log(this.formgroup.value.email, this.formgroup.value.password, ' Fields');
  
    const confirm = this.alertCtrl.create({
      title: 'Confirmation',
      message: 'Booked Successfully. Do you want to view your data',
      buttons: [
        {
          text: 'no',
          handler: () => {
            console.log('No clicked');
            this.no();
          }
        },
        {
          text: 'yes',
          handler: () => {
            console.log('Yes clicked');
            this.yes();
          }
        }      
      ]
    });
    confirm.present();
  }
  no() {
    this.navCtrl.setRoot('PatiententryPage');
  }
  yes() {
    this.navCtrl.push('ViewpatientPage');
  }
}
