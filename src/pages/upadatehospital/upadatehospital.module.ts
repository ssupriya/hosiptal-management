import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpadatehospitalPage } from './upadatehospital';

@NgModule({
  declarations: [
    UpadatehospitalPage,
  ],
  imports: [
    IonicPageModule.forChild(UpadatehospitalPage),
  ],
})
export class UpadatehospitalPageModule {}
