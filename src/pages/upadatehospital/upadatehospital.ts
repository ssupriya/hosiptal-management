import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the UpadatehospitalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-upadatehospital',
  templateUrl: 'upadatehospital.html',
})
export class UpadatehospitalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpadatehospitalPage');
  }
  update(){
    this.navCtrl.setRoot('ViewmyhospitalPage');
  }
}
