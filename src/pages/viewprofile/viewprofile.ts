import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import 'firebase/storage';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { FormGroup } from '@angular/forms';
import { AngularFireDatabase } from 'angularfire2/database';
import { DoctorslistPage } from '../doctorslist/doctorslist';
/**
 * Generated class for the ViewprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewprofile',
  templateUrl: 'viewprofile.html',
})
export class ViewprofilePage {
  private db = firebase.firestore();
  private data: FormGroup;
  profile: any[];
  profileList: any[];

  constructor(public navCtrl: NavController,
     public navParams: NavParams, 
     public alertCtrl: AlertController,
     public menuCtrl: MenuController,
     private database: AngularFireDatabase

     ) {
       this.getProfile();           
      this.menuCtrl.enable(false, 'default');
      this.menuCtrl.enable(false, 'patient');
      this.menuCtrl.enable(true, 'doctor');
      this.menuCtrl.enable(false, 'admin');
    }
    ionViewDidLoad() {
    console.log('ionViewDidLoad ViewprofilePage');
  }
   getProfile() {
    var profile =[]
     this.db.collection("doctordata").get().then((querySnapshot) => {
      querySnapshot.forEach((doc)=>{
       console.log(`${doc.id} => ${doc.data()}`);
       profile.push(doc.data());
      });       
     });
     this.profileList = profile;
     console.log(JSON.stringify(this.profile),'Profile');
    }
    update(){        
  const confirm = this.alertCtrl.create({
     title: 'Confirmation',
     message: 'Updated Successfully',
   buttons: [
      {
      text: 'OK',
        handler: () => {
       console.log('Ok clicked');
         this.ok();
        }
       },
        
     ]
   });
 confirm.present();
  }
  ok(){
    this.navCtrl.setRoot('AppointmentlistPage');
  }
}
