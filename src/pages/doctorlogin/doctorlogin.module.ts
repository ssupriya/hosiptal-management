import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoctorloginPage } from './doctorlogin';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    DoctorloginPage,
  ],
  imports: [ReactiveFormsModule,FormsModule,
    IonicPageModule.forChild(DoctorloginPage),
  ],
})
export class DoctorloginPageModule {}
