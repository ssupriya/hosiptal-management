import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

// import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
// import firebase from '@firebase/app';
// require('firebase/auth');

var isLogin = false;

@IonicPage()
@Component({
  selector: 'page-doctorlogin',
  templateUrl: 'doctorlogin.html',
})    
export class DoctorloginPage {
  private db = firebase.firestore();
  formgroup: FormGroup;
  email: any;
  password: any;
  users: any;
  loginT: any = '0';

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public formbuilder: FormBuilder,

  ) {
    this.formgroup = formbuilder.group({
      email: ['', [Validators.required, Validators.pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,3}")]],
      password: ['', [Validators.required, Validators.minLength(6)]],

    });
    // Enabling sidemenu's
    this.menuCtrl.enable(false, 'default');
    this.menuCtrl.enable(false, 'patient');
    this.menuCtrl.enable(false, 'doctor');
    this.menuCtrl.enable(false, 'admin');
    this.menuCtrl.enable(true, 'common');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DoctorloginPage');
  }


  // login(){
  //   console.log(this.formgroup.value.email,this.formgroup.value.password,' Fields');
  //   var mail = this.formgroup.value.email;
  //   var pwd = this.formgroup.value.password
  //   firebase.auth().signInWithEmailAndPassword(mail,pwd)
  //   .then((res)=>{
  //     console.log('a is', res, ' a in json')
  //   })
  //   console.log(this.formgroup.value.email,this.formgroup.value.password,' Fields')
  //   this.navCtrl.setRoot('AppointmentlistPage');
  // }
  async login() {

    console.log('form data', this.formgroup.value.email)
    // var em =  this.formgroup.value.email;
    // var ps =  this.formgroup.value.password;
    await this.db.collection("doctordata").where("email", "==", this.formgroup.value.email).where("password", "==", this.formgroup.value.password)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          // doc.data() is never undefined for query doc snapshots
          console.log(doc.id, " => ", doc.data());
          isLogin = true;
        });
      })
      .catch(function (error) {
        console.log("Error getting documents: ", error);
      });

    if (isLogin) {
      this.navCtrl.setRoot('AppointmentlistPage');
    } else {
      console.log('No details');
      alert('no user')
    }
  }
}
