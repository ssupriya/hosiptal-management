import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';

/**
 * Generated class for the ContactadminPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contactadmin',
  templateUrl: 'contactadmin.html',
})
export class ContactadminPage {

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public menuCtrl: MenuController,
    ) {
    this.menuCtrl.enable(false, 'default');
    this.menuCtrl.enable(false, 'patient');
    this.menuCtrl.enable(true, 'doctor');
    this.menuCtrl.enable(false, 'admin');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactadminPage');
  }
  report(){
    this.navCtrl.setRoot('AppointmentlistPage');
  }
}
