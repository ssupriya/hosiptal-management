import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { FilterProvider } from '../../providers/filter/filter';

/**
 * Generated class for the FilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html',
})
export class FilterPage {

    
 panchayat: any;
 Searchlist:any;
 constructor(public navCtrl: NavController,  public viewCtrl: ViewController, public navParams: NavParams,public loc:FilterProvider,public ev:Events) {
   this.loc.initializeState().then((res:any)=>{
     console.log(res);
     this.panchayat = res;
this.Searchlist=this.panchayat;
     console.log(this.panchayat)
   })
 }
 getItems(searchKey: any) {
   // Reset items back to all of the items
   // set val to the value of the searchbar
   this.Searchlist=this.panchayat;
console.log(this.Searchlist);
   const val = searchKey.target.value;
    console.log(val);
   // if the value is an empty string don't filter the items
   if (val && val.trim() != '') {
     this.Searchlist = this.panchayat.filter((item) => {
       return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
     });
   }
 
   }     
 dismiss(panchayats) {
  
 // var selectedDistricts = this.districts.filter(district => district.state_id == district.id)
 console.log(panchayats)     
   this.ev.publish('district',panchayats)    
   let data = { 'district': panchayats.name };
   this.viewCtrl.dismiss(panchayats);
 }      
 cancel(){
   this.viewCtrl.dismiss('')
 }

}

