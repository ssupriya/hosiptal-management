import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import * as firebase from 'firebase/app'; 
/**
 * Generated class for the AdminsignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()             
@Component({
  selector: 'page-adminsignup',
  templateUrl: 'adminsignup.html',
})
export class AdminsignupPage {
  formgroup:FormGroup;     
   email: any;
   password:any;       
   users:any;
  //isEnable:boolean = false;     
   //enable:boolean = false;
    constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public formbuilder :FormBuilder
 
    ) {
       this.formgroup = formbuilder.group({
        email:['',[Validators.required,Validators.pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,3}")]],
        password:['',[Validators.required,Validators.minLength(6)]],
      
     });         
       
      this.menuCtrl.enable(false, 'default');
      this.menuCtrl.enable(false, 'patient');
      this.menuCtrl.enable(false, 'doctor');
      this.menuCtrl.enable(false, 'admin');
      this.menuCtrl.enable(true, 'common');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminsignupPage');
  }
      login(){
       console.log(this.formgroup.value.email,this.formgroup.value.password,' Fields')
       var mail = this.formgroup.value.email;
    var pwd = this.formgroup.value.password
    firebase.auth().signInWithEmailAndPassword(mail,pwd)
    .then(()=>{
      this.navCtrl.push('ViewmyhospitalPage');
    })
    .catch(error => {alert(error)});
      //  this.navCtrl.push('ViewmyhospitalPage');
      // }
      // login(){
      //   this.navCtrl.setRoot('ViewmyhospitalPage');
      // }
}
}