import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminsignupPage } from './adminsignup';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AdminsignupPage,
  ],
  imports: [ReactiveFormsModule,FormsModule,
    IonicPageModule.forChild(AdminsignupPage),
  ],
})
export class AdminsignupPageModule {}
