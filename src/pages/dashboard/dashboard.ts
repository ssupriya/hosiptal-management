import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';

      
/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  ImageArray:any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public menuCtrl: MenuController) {
    this.ImageArray = [
      {'image':'assets/imgs/hos1.jpg'},
      {'image':'assets/imgs/hos2.jpg'},
      {'image':'assets/imgs/hos3.jpg'},
      {'image':'assets/imgs/hos4.jpg'},
      {'image':'assets/imgs/hos2.jpg'}
    ];


    this.menuCtrl.enable(true, 'default');
    this.menuCtrl.enable(false, 'patient');
    this.menuCtrl.enable(false, 'doctor');
    this.menuCtrl.enable(false, 'admin');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

 // search(){
   // this.navCtrl.setRoot('HossearchPage');
  //}
  about(){
    this.navCtrl.push('AboutimagePage');
  }
}




