import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


@Component({
  templateUrl: 'app.html'
})

export class MyApp {

  rootPage: any = 'DashboardPage';
  @ViewChild(Nav) nav: Nav;

  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,     
    public menuCtrl: MenuController) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });


    // Enabling sidemenu's
    this.menuCtrl.enable(true, 'default');
    this.menuCtrl.enable(false, 'patient');
    this.menuCtrl.enable(false, 'doctor');
    this.menuCtrl.enable(false, 'admin');
    this.menuCtrl.enable(false, 'common');


  }

  doctorlogin() {
    this.nav.push("DoctorloginPage");
  }

  admins() {
    this.nav.push("AdminsignupPage");
  }

  updateappointment() {
    this.nav.push('PatientupdateloginPage');
  }
  deleteappointment() {
    this.nav.push('DeleteappointmentPage');
  }
  // dashboard(){   
  //  this.nav.push('DashboardPage');
  //}
  update() {
    this.nav.push('UpadatehospitalPage');
  }
  addoctor() {
    this.nav.push('AddoctorPage');
  }
  logout() {
    this.nav.push('AdminsignupPage');
  }
  viewp() {
    this.nav.push('ViewprofilePage');
  }
  changep() {
    this.nav.push('ConfirmpwdPage');
  }
  contactadmin() {
    this.nav.push('ContactadminPage');
  }
  logoutdoc() {
    this.nav.push('DoctorloginPage');
  }
  dashboard() {
    this.nav.push('DashboardPage');
  }
  patient() {
    this.nav.setRoot('PatiententryPage');
  }
  hospital() {
    this.nav.push('ViewmyhospitalPage');
  }
  doctorlist(){
    this.nav.push('DoctorslistPage');
  }
  appointmentlist(){
    this.nav.setRoot('AppointmentlistPage');
  }
}
