import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { CalendarModule } from 'ionic3-calendar-en';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx'; 
import { Camera} from '@ionic-native/camera/ngx';     
import { CameraProvider } from '../providers/camera/camera';
import { FilterProvider } from '../providers/filter/filter';
import { HttpClientModule } from '@angular/common/http';
import { Calendar } from '@ionic-native/calendar/ngx';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import firebase from 'firebase';
const config = {
  apiKey: "AIzaSyD8DeeAdcTedUSt1m6OmRl6LFvDuwiG4nY",
  authDomain: "college-fcdec.firebaseapp.com",
  databaseURL: "https://college-fcdec.firebaseio.com",
  projectId: "college-fcdec",
  storageBucket: "college-fcdec.appspot.com",
  messagingSenderId: "162007357046"
};

firebase.initializeApp(config);
@NgModule({
  declarations: [                
    MyApp,         
    HomePage              
  ],    
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    CalendarModule,ReactiveFormsModule,
    FormsModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(config),
    AngularFireAuthModule
  ],    
  bootstrap: [IonicApp],         
  entryComponents: [
    MyApp,
    HomePage     
  ],

  providers: [
    StatusBar,
    SplashScreen,
    FileTransfer,
     File,
    Camera,
    Calendar,    
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CameraProvider,
    FilterProvider,HttpClientModule
  ]
})
export class AppModule {}
    